class UserEntity{
  late String? id;
  late String? email;
  late String? password;
  late bool? isAdmin;

  UserEntity(
    {
      this.id,
      this.email,
      this.isAdmin,
      this.password,
    }
  );

}