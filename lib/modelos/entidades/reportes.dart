
import 'package:cloud_firestore/cloud_firestore.dart';

class ReportEntity{
  late String? id;
  late String? titulo;
  late String? descricion;
  late String? direccion;
  late String? foto;
  late String? usuario;
  late String? fecha;

  ReportEntity(
    { 
      this.titulo,
      this.descricion,
      this.direccion,
      this.foto,
      this.usuario,
      this.fecha,
      this.id
    } 
  );

  factory ReportEntity.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options){
      var data = snapshot.data();

      return ReportEntity(
        id:snapshot.id,
        titulo: data?["titulo"],
        descricion: data?["descricion"],
        direccion: data?["direccion"],
        foto: data?["foto"],
        usuario: data?["usuario"],
        fecha: data?["fecha"]
      );

    }
  


  Map<String, dynamic> toFirestore(){
    return {
      if (titulo != null && titulo!.isNotEmpty )"titulo":titulo,
      if (descricion!= null && descricion!.isNotEmpty)"descricion":descricion,
      if (direccion!= null && direccion!.isNotEmpty )"direccion":direccion,
      if (foto!= null && foto!.isNotEmpty )"foto":foto,
      if (usuario!= null && usuario!.isNotEmpty)"usuario":usuario,
      if (fecha!= null && fecha!.isNotEmpty )"fecha":fecha
    };
  }

  @override
  String toString(){
      return "Report(titulo: $titulo, descricion: $descricion, direccion: $direccion, foto: $foto,usuario : $usuario, fecha : $fecha)";
  }

}