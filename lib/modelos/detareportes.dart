//tiene lac clases que van a hacer referencia a la categoria

class Detareportes {
  int id;
  String nombre;
  String foto;

  Detareportes(this.id, this.nombre, this.foto);
}

// usamos un final para inflar los datos a traves de un api rest

// ignore: non_constant_identifier_names
final Datos = [
  Detareportes(
      1,
      "Accidentes: accitende en el barrio yomasa a las 12:01 de hoy",
      "logoa.png"),
  Detareportes(
      2,
      "Corte Servicios: Corte de servicion del gas por 24 horas en el barrio Marsella",
      "logoc.png"),
  Detareportes(
      3, "Huecos: Hueco en la calle 12 con 27, tener precaución", "logoh.png"),
  Detareportes(
      4,
      "Trancones: Trancon por arreglos en la calle 80 velocidad max de 20 km",
      "logot.png"),
];
