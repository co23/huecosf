//tiene lac clases que van a hacer referencia a la categoria

class Categoria {
  int id;
  String nombre;
  String foto;

  Categoria(this.id, this.nombre, this.foto);
}

// usamos un final para inflar los datos a traves de un api rest

// ignore: non_constant_identifier_names
final Menu = [
  Categoria(1, "Accidentes", "logoa.png"),
  Categoria(2, "Corte Servicios", "logoc.png"),
  Categoria(3, "Huecos", "logoh.png"),
  Categoria(4, "Trancones", "logot.png"),
];
