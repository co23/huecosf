
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:huecos/modelos/entidades/reportes.dart';

class ReportRepository{

  late final CollectionReference _colletion;

  ReportRepository(){
    _colletion = FirebaseFirestore.instance.collection("reportes");
  }

  Future<void> newReport(ReportEntity report) async{
    await _colletion.withConverter(
      fromFirestore: ReportEntity.fromFirestore, 
      toFirestore: (value, options) => value.toFirestore()
    ).add(report);
  }

  Future<List<ReportEntity>> getAllReportsByEmail(String email) async{
    var query = await _colletion
        .where("usuario", isEqualTo: email)
        .orderBy("fecha") 
        .withConverter<ReportEntity>(
            fromFirestore: ReportEntity.fromFirestore, 
            toFirestore: (value, options) => value.toFirestore())
        .get();

    var sales = query.docs.cast().map<ReportEntity>((e){
      var sale = e.data();
      sale.id=e.id;
      return sale;
    });

    return sales.toList();

  }

  Future<List<ReportEntity>> getAllReports() async{
    var query = await _colletion
    .orderBy("fecha")  
    .withConverter(
      fromFirestore: ReportEntity.fromFirestore, 
      toFirestore: (value, options) => value.toFirestore()
    ).get();

    var  reportes = query.docs.cast().map<ReportEntity>((e){
      var reporte = e.data();
      reporte.id=e.id;

      return reporte;
    });

    return reportes.toList();
  }
}

/* import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:huecos/modelos/entidades/reportes.dart';

class ReportRepository{
  late final FirebaseFirestore db; 

  ReportRepository(){
    db = FirebaseFirestore.instance;
  }

  void save(ReportEntity report){
    db.collection("reportes").doc().set(report.toFirestore());
  }

} */