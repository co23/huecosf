import 'package:flutter/material.dart';
import 'package:huecos/pages/formulario.dart';
import 'package:huecos/pages/login.dart';
import 'package:huecos/pages/reportes_todo.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '../controlador/login.dart';
import '../pages/reportes.dart';



//GlobalKey<ScaffoldState> _key2 = GlobalKey<ScaffoldState>();
class DrawerWidget extends  StatefulWidget{

  DrawerWidget({super.key});

  @override
  State<DrawerWidget> createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final _preferens = SharedPreferences.getInstance();
  final _loginController = LoginController();

  String _email = "";

  @override
  void initState() {
    super.initState();
    _preferens.then((pref){
      setState(() {
        _email = pref.getString("userEmail") ?? "N/A";
      });   
    });
  }

  @override
  Widget build(BuildContext context){
    return _drawer();

  }

  Widget _drawer(){
    return Drawer(
      child: Container(
        color: Colors.green[700],
        child: ListView(
          padding: EdgeInsets.zero,
            children: [
              const SizedBox(height: 60),
              SizedBox(
                height: 250,
                child:
                  DrawerHeader(
                  margin :  const EdgeInsets.only(bottom: 18.0),
                  padding: EdgeInsets.zero,
                  decoration:  BoxDecoration(
                    color: Colors.green[400],
                  
                  ),
                  child: _header(),
                ),  
              ),  
              const SizedBox(height: 60),
              ListTile(

                leading: const Icon(Icons.home, size: 45, color: Colors.white),
                title: const  Text('Inicio', style: TextStyle(fontSize: 24, color: Colors.white),),
                onTap: (){
                  var nav = Navigator.of(context);
                  nav.push(
                    MaterialPageRoute(
                      builder: (context)=> const ReportesTodo())
                  );
                },
              ),
              const SizedBox(height: 30),
              ListTile(
                leading: const Icon(Icons.message, size: 45, color: Colors.white),
                title: const  Text('Reportar', style: TextStyle(fontSize: 24, color: Colors.white),),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context)=> Formulario())
                  );
                },
              ),
              const SizedBox(height: 30),
              ListTile(
                leading: const Icon(Icons.playlist_add_check, size: 45, color: Colors.white),
                title: const  Text('Reportados', style: TextStyle(fontSize: 24, color: Colors.white),),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context)=> Reportes())
                  );
                },
              ),
              const SizedBox(height: 30),
              ListTile(
                
                leading: const Icon(Icons.exit_to_app, size: 45, color: Colors.white),
                title: const  Text('Salir', style: TextStyle(fontSize: 24, color: Colors.white),),
                onTap: () async{
                  var nav = Navigator.of(context);
                  _loginController.logOut();
                  var pref = await _preferens; 
                  pref.remove("userEmail");
                  nav.pushReplacement(
                      MaterialPageRoute(
                        builder: (context)=>Login()
                      )
                  );
                },
              ),
              const SizedBox(height: 30),
            ],

        ),
      ),
    );
  }

  Widget _header()  {
    const iconoUsuario = Icon(Icons.account_box, size: 50,  color: Colors.white); 
    return Center(
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children:[
           CircleAvatar(
            backgroundColor: Colors.green[800],
            radius: 40,
            child: iconoUsuario,
          ),
          const SizedBox(height: 18),
          const Text("correo", style: TextStyle(color: Colors.white , fontSize: 20)),
          const SizedBox(height: 18),
          Text( _email, style: const TextStyle(color: Colors.white , fontSize: 20)),
        ]
      ),
    );
  }
}