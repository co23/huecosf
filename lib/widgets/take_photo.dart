import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:huecos/modelos/entidades/reportes.dart';
import 'package:huecos/pages/take_photo_page.dart';

class TakePhotoW extends StatefulWidget {
  final ReportEntity report;
  const TakePhotoW({super.key, required this.report});

  @override
  _TakePhotoStateW  createState() => _TakePhotoStateW ();
}

class _TakePhotoStateW  extends State<TakePhotoW > {
  String? _foto;

  @override
  Widget build(BuildContext context) {
    Widget icono;
    if(widget.report.foto ==  null){
        icono = Column(
        children:[
          const Text(' 📷 Toma Una Foto 📷 ', style:TextStyle(fontSize: 22)),
          const SizedBox(width: 40,height: 10,),
          IconButton(
                icon: const  Icon(Icons.camera_alt, size: 60),
                iconSize: 50,
                onPressed: () async{
                  
                  var nav  = Navigator.of(context);
                  final cameras = await availableCameras();
                  final camera  = cameras.last;

                  var imagePath = await nav.push<String>(
                    MaterialPageRoute(builder: 
                        (context) => TakePhotoPage(
                          camera: camera
                        )
                      )
                  );
                  if(imagePath != null && imagePath.isNotEmpty){
                    setState((){
                      widget.report.foto = imagePath;
                    });
                  }
                },
          ),
    ]
    );
    }else{
      icono = ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),//add border radius here
          child: Image(image: FileImage(File(widget.report.foto!))),//add image location here
        );
    }
    return icono;
  }  
}