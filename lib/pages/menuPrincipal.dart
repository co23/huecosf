import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:huecos/modelos/categoria.dart';
import 'package:huecos/pages/reportes.dart';

class MenuPrincipal extends StatelessWidget {
  const MenuPrincipal({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: SvgPicture.asset(
            "assets/icons/huecoicon.svg",
            width: 5,
            height: 5,
            color: Colors.white,
          ),
          title: const Text("Menu Principal"),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.menu),
            ),
          ]),
      backgroundColor: const Color.fromARGB(255, 79, 149, 0),
      // ignore: avoid_unnecessary_containers
      body: Container(
        child: GridView.builder(
            itemCount: Menu.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2),
            itemBuilder: (context, index) {
              return Container(
                  margin: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => const Reportes()));
                      /* print(
                          "click en${Menu[index].nombre}"); //para probar en la consola*/
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // ignore: prefer_interpolation_to_compose_strings

                        Image.asset(
                          "assets/imagenes/${Menu[index].foto}",
                          width: 151,
                        ),
                        Text(Menu[index].nombre),
                      ],
                    ),
                  ));
            }),
      ),
    );
  }
}
