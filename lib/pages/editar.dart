import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:huecos/pages/menuPrincipal.dart';

class Editar extends StatelessWidget {
  const Editar({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 24, 210, 33),
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: SvgPicture.asset(
            "assets/icons/huecoicon.svg",
            width: 5,
            height: 5,
            color: Colors.white,
          ),
          title: const Text("Editar Reporte"),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.menu),
            ),
          ]),
      body: Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(50),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Form(
            child: Column(
              children: <Widget>[
                _crearInput1(),
                const SizedBox(
                  width: 40,
                  height: 20,
                ),
                _crearInput2(),
                const SizedBox(
                  width: 40,
                  height: 20,
                ),
                _crearInput3(),
                const SizedBox(
                  width: 40,
                  height: 20,
                ),
                _crearInput4(),
                const SizedBox(height: 89),
                _guardar(),
                const SizedBox(height: 50),
                _cancelar(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearInput1() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: TextFormField(
        obscureText: true,
        decoration: const InputDecoration(
            labelText: 'Titulo',
            icon: Icon(Icons.title),
            hintText: 'Titulo denuncia'),
      ),
    );
  }

  Widget _crearInput2() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: TextFormField(
        obscureText: true,
        decoration: const InputDecoration(
            labelText: 'Descripción',
            icon: Icon(Icons.abc),
            hintText: 'Escriba datos exactos de interes'),
      ),
    );
  }

  Widget _crearInput3() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: TextFormField(
        obscureText: true,
        decoration: const InputDecoration(
            labelText: 'Dirección',
            icon: Icon(Icons.add_location_alt_sharp),
            hintText: 'Ingrese la ubicacion'),
      ),
    );
  }

  Widget _crearInput4() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: TextFormField(
        obscureText: true,
        decoration: const InputDecoration(
            labelText: 'Foto',
            icon: Icon(Icons.camera_alt_outlined),
            hintText: 'Imagen comprobante'),
      ),
    );
  }

  Widget _guardar() {
    return ElevatedButton(
      style: TextButton.styleFrom(
        backgroundColor: Colors.green,
        padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 15),
      ),
      onPressed: () {
        /*Navigator.push(
            context, MaterialPageRoute(builder: (_) => new MenuPrincipal()));*/
      },
      child: const Text("Actualizar", style: TextStyle(fontSize: 20)),
    );
  }

  Widget _cancelar() {
    return ElevatedButton(
      style: TextButton.styleFrom(
        backgroundColor: Colors.green,
        padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 15),
      ),
      onPressed: () {
        /*Navigator.push(
            context, MaterialPageRoute(builder: (_) => new MenuPrincipal()));*/
      },
      child: const Text("Cancelar", style: TextStyle(fontSize: 20)),
    );
  }
}
