import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:huecos/controlador/reportes.dart';
import 'package:huecos/modelos/detareportes.dart';
import 'package:huecos/modelos/entidades/reportes.dart';
import 'package:huecos/widgets/drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Reportes extends StatefulWidget {


  const Reportes({super.key});

  @override
  State<Reportes> createState() => _ReportesState();
}

class _ReportesState extends State<Reportes> {
  GlobalKey<ScaffoldState> _key2 = GlobalKey<ScaffoldState>();

  final _preferens = SharedPreferences.getInstance();
  final  _reporContoller = ReportController();
  List<ReportEntity> _lista = [];
  String _email = "";

  @override
  void initState() {
    super.initState();
    _preferens.then((pref) async{
      _email = pref.getString("userEmail") ?? "N/A";

      _reporContoller.listAll(_email).then((value){
        setState((){
          _lista =value;
        });
      });  
    });
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Scaffold(
        backgroundColor: const Color.fromARGB(255, 24, 210, 33),
        key: _key2,
        appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            centerTitle: true,
            leading: SvgPicture.asset(
              "assets/icons/huecoicon.svg",
              width: 5,
              height: 5,
              color: Colors.white,
            ),
            title: const Text("Reportes"),
            actions: [
              IconButton(
                
                onPressed: () => _key2.currentState!.openDrawer(),
                icon: const Icon(Icons.menu),
              ),
            ] 
            ),
        drawer: DrawerWidget(),    
        body: Container(
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(11),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: ListView.builder(
              itemCount: _lista.length,
              itemBuilder: (context, index) => ListTile(
              leading: _lista[index].foto == null 
                ?  Image.network(
                  "https://www.elpais.com.co/files/article_main/uploads/2020/02/28/5e597765a9e73.png",
                  height: 200, 
                  width: 80,
                ):
                Image.network(
                  _lista[index].foto!,
                  height: 200, 
                  width: 80,
                  fit: BoxFit.fitWidth,
                ),
              title:  Text(_lista[index].titulo!, 
                        style: const  TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25,
                                      )
                      ),
              subtitle: Text(_lista[index].descricion!,
                          style: const TextStyle(fontSize: 15)
                        ),

              ),
            ),
        ),
        
      ),
    );
  }
}

/* Widget _item_left(String texto, String imagen) {
  return Row(
    children: [
      Expanded(flex: 1, child: Text(texto)),
      Expanded(
          flex: 1,
          child: Image.asset(
            "assets/imagenes/$imagen",
            width: 150,
          )),
    ],
  );
} */

/* Widget _item_right(String texto, String imagen) {
  return Row(
    children: [
      Expanded(
          flex: 1,
          child: Image.asset(
            "assets/imagenes/$imagen",
            width: 150,
          )),
      Expanded(flex: 1, child: Text(texto)),
    ],
  );
} */
