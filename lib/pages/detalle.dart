import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:huecos/pages/menuPrincipal.dart';

class Detalle extends StatelessWidget {
  final _imagenejemplo = "assets/imagenes/hueco.jpg";
  const Detalle({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: SvgPicture.asset(
            "assets/icons/huecoicon.svg",
            width: 5,
            height: 5,
            color: Colors.white,
          ),
          title: const Text("Detalles"),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.menu),
            ),
          ]),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Column(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const Text("Hueco Terrible Cuidado",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
              const SizedBox(height: 30),
              const Text("03/11/2022",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                  )),
              const SizedBox(height: 5),
              Image.asset(_imagenejemplo),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: Column(
                  children: [
                    const SizedBox(height: 30),
                    const Text("2972 Westheimer Rd. Santa Ana, Illinois 85486",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic)),
                    const SizedBox(height: 40),
                    const Text(
                        "Amet Minim Mollit Non Deserunt Ullamco Est Sit ALiqua Dolor Do Amet Sint. Velit Officia Consequat Duis Enim Velit Mollit. Exercitation Veniam Consequat Sunt Nostrud Amet.",
                        style: TextStyle(fontSize: 20)),
                    const SizedBox(height: 100),
                    FloatingActionButton(
                      onPressed: () {},
                      backgroundColor: Colors.green,
                      child: const Icon(Icons.add),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
