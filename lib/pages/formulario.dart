import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:huecos/controlador/reportes.dart';
import 'package:huecos/modelos/entidades/reportes.dart';
import 'package:huecos/modelos/repositorio/reportes.dart';
import 'package:huecos/pages/menuPrincipal.dart';
import 'package:huecos/pages/reportes.dart';
import 'package:huecos/pages/reportes_todo.dart';
import 'package:huecos/widgets/take_photo.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/drawer.dart';


class Formulario extends StatelessWidget {
  GlobalKey<ScaffoldState> _key3 = GlobalKey<ScaffoldState>();
  final _preferens = SharedPreferences.getInstance();
  late ReportEntity _reporte;
  late ReportController _controller;
  //late var _fecha;
  Formulario({super.key}){
    _reporte = ReportEntity();
    _controller = ReportController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 24, 210, 33),
        key: _key3,
        appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            centerTitle: true,
            leading: SvgPicture.asset(
              "assets/icons/huecoicon.svg",
              width: 5,
              height: 5,
              color: Colors.white,
            ),
            title: const Text("Reportes"),
            actions: [
              IconButton(
                
                onPressed: () => _key3.currentState!.openDrawer(),
                icon: const Icon(Icons.menu),
              ),
            ] 
            ),
        drawer: DrawerWidget(),
      body: Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(50),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _formulario(context),
              const SizedBox(height: 20),
              _cancelar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _formulario(BuildContext context){
    final formKey = GlobalKey<FormState>();

    return  Form(
              key: formKey, 
              child: Column(
                children: [
                  _crearInput1(),
                  const SizedBox(
                    width: 40,
                    height: 10,
                  ),
                  _crearInput2(),
                  const SizedBox(
                    width: 40,
                    height: 10,
                  ),
                  _crearInput3(),
                  const SizedBox(
                    width: 40,
                    height: 10,
                  ),
                  _crearInput4(),
                  const SizedBox(
                    width: 40,
                    height: 30,
                  ),
                  
                  ElevatedButton(  
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.green,
                      padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 15),
                    ),
                    onPressed: () async{
                      var nav = Navigator.of(context);
                      if(formKey.currentState!.validate()){
                        formKey.currentState!.save();
                        try{
                          var pref = await _preferens;
                          //_controller.saveReport();
                          //_fecha = DateTime.now();
                          _reporte.fecha =  DateTime.now().toString();
                          _reporte.usuario =  pref.getString("userEmail");
                          await _controller.save(_reporte);

                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: 
                              Text("Reporte Guardado")
                            ),
                          );

                          nav.push(
                              MaterialPageRoute(
                            builder: (context)=> const ReportesTodo())
                          );
                          
                        }catch(e){
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: 
                              Text("error: $e")
                            ),
                          );
                        }
                      }

                    },
                    child: const Text("Guardar", style: TextStyle(fontSize: 20)),
                  ),
                ],
              ),
            );
  }

  Widget _crearInput1() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: TextFormField(
        onSaved: (newValue){
          _reporte.titulo = newValue;
        },
        validator: (value) {
          if(value== null || value.isEmpty){
            return 'faltan datos';
          }
          return null;
        },
        maxLength: 50,
        decoration: const InputDecoration(
            labelText: 'Titulo',
            icon: Icon(Icons.title),
            hintText: 'Titulo denuncia'),
      ),
    );
  }

  Widget _crearInput2() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: TextFormField(
        onSaved: (newValue){
          _reporte.descricion = newValue;
        },
        validator: (value) {
          if(value== null || value.isEmpty){
            return 'faltan datos';
          }
          return null;
        },
        keyboardType: TextInputType.multiline,
        maxLength: 250,
        minLines: 1,//Normal textInputField will be displayed
        maxLines: 6,// when user presses enter it will adapt to it
        decoration: const InputDecoration(
            labelText: 'Descripción',
            icon: Icon(Icons.abc),
            hintText: 'Describe como tu quieras '),
      ),
    );
  }

  Widget _crearInput3() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: TextFormField(
        onSaved: (newValue){
          _reporte.direccion = newValue;
        },
        validator: (value) {
          if(value== null || value.isEmpty){
            return 'faltan datos';
          }
          return null;
        },
        decoration: const InputDecoration(
            labelText: 'Dirección',
            icon: Icon(Icons.add_location_alt_sharp),
            hintText: 'Ingrese la ubicacion'),
      ),
    );
  }

  Widget _crearInput4() {
    return TakePhotoW(report: _reporte);
  }

  Widget _cancelar() {
    return ElevatedButton(
      style: TextButton.styleFrom(
        backgroundColor: Colors.red[500],
        padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 15),
        tapTargetSize: MaterialTapTargetSize.shrinkWrap
      ),
      onPressed: () {
        /*Navigator.push(
            context, MaterialPageRoute(builder: (_) => new MenuPrincipal()));*/
      },
      child: const Text("Cancelar", style: TextStyle(fontSize: 20)),
    );
  }
}
