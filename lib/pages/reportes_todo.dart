import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../controlador/reportes.dart';
import '../modelos/entidades/reportes.dart';
import '../widgets/drawer.dart';


//GlobalKey<FormState> _abcKey = GlobalKey<FormState>();
class ReportesTodo extends StatefulWidget {
  const ReportesTodo({ Key? key }) : super(key: key);

  @override
  _ReportesTodoState createState() => _ReportesTodoState();
}

class _ReportesTodoState extends State<ReportesTodo> {
  GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  final _preferens = SharedPreferences.getInstance();
  final  _reporContoller = ReportController();
  String _email = "";
  List<ReportEntity> _lista = [];

  @override
  void initState() {
    super.initState();
    _listarReportes();
  
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
       backgroundColor: const Color.fromARGB(255, 24, 210, 33),
        key: _key,
        appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            centerTitle: true,
            leading: SvgPicture.asset(
              "assets/icons/huecoicon.svg",
              width: 5,
              height: 5,
              color: Colors.white,
            ),
            title: const Text("Ultimos Reportes"),
            actions: [
              IconButton(
                
                onPressed: () => _key.currentState!.openDrawer(),
                icon: const Icon(Icons.menu),
              ),
            ] 
            ),
        drawer: DrawerWidget(),  
        body: Container(
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(11),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: ListView.builder(
              itemCount: _lista.length,
              itemBuilder: (context, index) => ListTile(
               leading: _lista[index].foto == null 
                ?  Image.network(
                  "https://www.elpais.com.co/files/article_main/uploads/2020/02/28/5e597765a9e73.png",
                  height: 200, 
                  width: 80,
                ):
                Image.network(
                  _lista[index].foto!,
                  height: 200, 
                  width: 80,
                  fit: BoxFit.fitWidth,
                ),
                 /* Image(
                  image: FileImage(File(_lista[index].foto!)),
                  height: 200, 
                  width: 80,
                  fit: BoxFit.fitWidth,
                 ), */
              /* if(_lista[index].foto == null || _lista[index].foto.isEmpty){

              }else{
                leading: Image(image: FileImage(File(_lista[index].foto!))),
              }    */
              /* leading: Image.network(
                  "https://www.elpais.com.co/files/article_main/uploads/2020/02/28/5e597765a9e73.png",
                  height: 200, 
                  width: 80,
                ), */
              title:  Text(_lista[index].titulo!, 
                        style: const  TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25,
                                      )
                      ),
              subtitle: Text(_lista[index].descricion!,
                          style: const TextStyle(fontSize: 15)
                        ),
              shape: const Border(bottom: BorderSide(
                  color:Color(0x0000ffff), 
                  width: 1,
                  style: BorderStyle.solid
                  ),
                ),        
              ),
            ),
        ),
    );
  }

  void _listarReportes(){
    _preferens.then((pref) async{
        _email = pref.getString("userEmail") ?? "N/A";
      _reporContoller.listAllReports().then((value){
          setState((){
            _lista =value;
          });
      });
    });

  }
}