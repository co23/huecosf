import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:huecos/controlador/reportes.dart';

import '../modelos/entidades/reportes.dart';
import '../modelos/repositorio/reportes.dart';


final GlobalKey<ScaffoldState> _key = GlobalKey();

class Prueba extends StatefulWidget {
  const Prueba({ Key? key }) : super(key: key);

  @override
  _PruebaState createState() => _PruebaState();
}

class _PruebaState extends State<Prueba> {
  final _preferens = SharedPreferences.getInstance();
  final  _reporContoller = ReportController();
  List<ReportEntity> _lista = [];
  String _email = "";

  @override
  void initState() {
    super.initState();
    _preferens.then((pref) async{
      _email = pref.getString("userEmail") ?? "N/A";

      _reporContoller.listAll(_email).then((value){
        setState((){
          _lista =value;
        });
      });  
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:  const Text("probando")),
      body: Text(
        _email
      ),
    );
  }
}