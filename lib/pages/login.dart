import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:huecos/controlador/login.dart';
import 'package:huecos/controlador/request/login_request.dart';
import 'package:huecos/pages/prueba.dart';
import 'package:huecos/pages/registro.dart';
import 'package:huecos/pages/reportes.dart';
import 'package:huecos/pages/reportes_todo.dart';
import 'package:shared_preferences/shared_preferences.dart';



class Login extends StatelessWidget {
  final _imagenLogin = "assets/imagenes/fondologin.png";
  final _preferens = SharedPreferences.getInstance();
  late LoginController _controller;
  late LoginRequest _request;

  Login({super.key}){
    _controller = LoginController();
    _request = LoginRequest();

  }
  

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: SvgPicture.asset("assets/icons/huecoicon.svg",
            width: 5,
            height: 5,
            color: Colors.white,
          ),
          
          title: const Text("Inicio"),
          /*actions: [
          IconButton(
                onPressed:(){},
                icon: const Icon(Icons.menu),
              ),
          ]*/

        ),
        body: 
          Padding(
            padding: const EdgeInsets.all(1.0),
            child: SingleChildScrollView(
              child: Column(
                children:[
                  Image.asset(_imagenLogin),
                  Padding(
                    padding: const EdgeInsets.all(6.0), 
                    child:
                    Column(
                      children:[
                          /* const SizedBox(height: 20),
                          _campoEmail(),
                          const SizedBox(height: 20),
                          _campoClave(),
                          const SizedBox(height: 10),
                          _botonLogin(),
                          const SizedBox(height: 30),
                          _botonesLoginRedes(),
                          const SizedBox(height: 50), */
                          _formulario(context),
                          TextButton(onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context)=>Registro()
                              )
                            );
                          }, 
                            style: TextButton.styleFrom(
                              minimumSize: Size.zero,
                              padding: EdgeInsets.zero,
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              
                            ),
                            
                            child: const Text("Registrate", 
                                    style: TextStyle(fontSize: 22)
                                  ),
                          ),
                          const SizedBox(height: 30),
                          TextButton(onPressed: (){}, 
                            style: TextButton.styleFrom(
                              minimumSize: Size.zero,
                              padding: EdgeInsets.zero,
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            ),
                            child: const Text("¿olvidastes tu contraseña?",
                            style: TextStyle(fontSize: 22)
                            ),
                          ),
                        ],
                      ),
                ),
              ],
            ),
          ),  
          ),
        );
  }

  Widget _formulario (BuildContext context){
    final formKey = GlobalKey<FormState>();
    return Form(
      key:formKey,
      child: Column(
          children: [
            const SizedBox(height: 20),
            _campoEmail(),
            const SizedBox(height: 20),
            _campoClave(),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:50.0),
              child: _botonLogin(context,formKey)
            ),
            const SizedBox(height: 30),
            _botonesLoginRedes(),
            const SizedBox(height: 50),
          ],
        ),
      );
  }

  Widget _campoEmail(){
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal:50.0),
        child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            decoration: const InputDecoration(
              prefixIcon: Icon(Icons.account_box),
              border: OutlineInputBorder(),
              labelText:  "Correo Electronico", 
              contentPadding: EdgeInsets.symmetric(
                vertical: 5,
                horizontal:15
              ),
            ),
            onSaved: (value){
              _request.email = value!;
            },
            validator: (value) {
              if (value == null || value.isEmpty){
                return 'el campo esta  vacio';
              }
              if (!value.contains("@") || !value.contains(".")) {
                return "El correo tiene un formato inválido";
              }
              return null ;
            },
        ),
      );
  }

  Widget _campoClave(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50.0),
      child: TextFormField(

          obscureText:true,        
          decoration: const InputDecoration(
          prefixIcon: Icon(Icons.lock),
          border: OutlineInputBorder(),
          labelText:  "Contraseña",
          contentPadding: EdgeInsets.symmetric(
                vertical: 5,
                horizontal:15
          ), 
        ),
        onSaved: (value){
              _request.password = value!;
            },
          validator: (value) {
            if (value == null || value.isEmpty){
              return 'el campo esta  vacio';
            }
            
            return null ;
          },
      ),
    );
  }
 
  
  Widget _botonesLoginRedes() {
     return Row(
       mainAxisAlignment: MainAxisAlignment.spaceAround, 
       children: [
         ElevatedButton(
            style: TextButton.styleFrom(
                backgroundColor: Colors.blue,
                 padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 25),
            ),
            onPressed: () { },
            child: const Icon(Icons.facebook, size : 50),
          ),
          ElevatedButton(
            style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
            ),
            onPressed: () { },
            child: const Icon(Icons.g_mobiledata_sharp, size: 60),
          ),
       ],
     );
  }

  Widget _botonLogin(BuildContext context, GlobalKey<FormState> formKey){
    return  
      ElevatedButton(
        style: TextButton.styleFrom(
          backgroundColor: Colors.green,
          padding: const EdgeInsets.symmetric(vertical: 25),
          minimumSize: const Size.fromHeight(50),
        ),
        onPressed: () async { 
          var nav = Navigator.of(context);
          if(formKey.currentState!.validate()){
            formKey.currentState!.save();
            // validar todos los campos
            try{
              var user = await _controller.validarCorreoClave(_request);

              var pref = await _preferens;
              pref.setString("userEmail",user); 

              nav.pushReplacement( 
                    MaterialPageRoute(
                      builder:(context)=>ReportesTodo()
                    )
              );
            }catch (e){
              ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(e.toString()),
                  )
                );
            }
            
          }
        },
        child: const Text("Ingresar",
            style:TextStyle(fontSize:20)
        ),
      );
  }
}