import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Recuperar extends StatelessWidget {
  final _imagenBloqueo = "assets/imagenes/bloqueado.png";
  const Recuperar({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: SvgPicture.asset(
            "assets/icons/huecoicon.svg",
            width: 5,
            height: 5,
            color: Colors.white,
          ),
          title: const Text("R.Contraseña"),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.menu),
            ),
          ]),
      body: Padding(
        padding: const EdgeInsets.all(21.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 60),
              Image.asset(_imagenBloqueo, height: 200),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    const SizedBox(height: 10),
                    _campoEmail(),
                    const SizedBox(height: 30),
                    _botonRegistro(),
                    const SizedBox(height: 80),
                    TextButton(
                        onPressed: () {},
                        style: TextButton.styleFrom(
                          minimumSize: Size.zero,
                          padding: EdgeInsets.zero,
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                        child: const Text("Registrarse",
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: 20))),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _campoEmail() {
    return TextFormField(
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.mail),
        border: OutlineInputBorder(),
        labelText: "Correo Electronico",
        contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      ),
    );
  }

  Widget _botonRegistro() {
    return ElevatedButton(
      style: TextButton.styleFrom(
        backgroundColor: Colors.green,
        minimumSize: const Size.fromHeight(50),
      ),
      onPressed: () {},
      child: const Text("Recuperar Contraseña", style: TextStyle(fontSize: 20)),
    );
  }
}
