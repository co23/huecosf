import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TakePhotoPage extends StatefulWidget {
  final CameraDescription camera;
  const TakePhotoPage({ super.key,required this.camera});

  @override
  _TakePhotoPageState createState() => _TakePhotoPageState();
}

class _TakePhotoPageState extends State<TakePhotoPage> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;


  @override
  void initState() {
    super.initState();
    _controller = CameraController(widget.camera, ResolutionPreset.medium);
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  const Text("toma una foto"),
      ),
      body: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot)  {
            if(snapshot.connectionState == ConnectionState.done){
              return Column(
                children: [
                  Expanded(child: CameraPreview(_controller))
                ],
              );
            }else{
              return const Center(child: CircularProgressIndicator(),);
            }
          },  
        ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          
          try{
            final nav = Navigator.of(context);
            await _initializeControllerFuture; 
            final image = await _controller.takePicture();
            if(!mounted) return;
            nav.pop(image.path);
          }catch(e){
            ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content:Text("$e")));
          }
      },
      child: const Icon(Icons.camera),
      ) ,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}