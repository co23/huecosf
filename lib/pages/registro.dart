import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:huecos/controlador/login.dart';
import 'package:huecos/modelos/detareportes.dart';
import 'package:huecos/controlador/request/register.dart';
import 'package:huecos/pages/reportes.dart';

class Registro extends StatelessWidget {
  late RegisterRequest _datos;
  late LoginController _controller;
  final _imagenLogin = "assets/imagenes/user1.png";
  

  Registro({super.key}){
    _datos = RegisterRequest();
    _controller = LoginController();
   
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    late String rePassword;

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          leading: SvgPicture.asset(
            "assets/icons/huecoicon.svg",
            width: 5,
            height: 5,
            color: Colors.white,
          ),
          title: const Text("Crear Cuenta"),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.menu),
            ),
          ]),
      body: Padding(
        padding: const EdgeInsets.all(21.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 60),
              Image.asset(_imagenLogin, height: 160),
              const SizedBox(height: 60),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: Form(
                  key:formKey,
                  child: Column(
                    children: [
                      /* const SizedBox(height: 20),
                      _campoEmail(),
                      const SizedBox(height: 10),
                      _campoClave(),
                      const SizedBox(height: 10),
                      _campoConfClave(), */
                      _basicWidget(
                        "Correo Electronico", 
                        validarCampoObligatorio, 
                        (newValue) { 
                          _datos.email = newValue!; 
                        },
                        const Icon(Icons.mail),
                        
                      ),
                      const SizedBox(height: 10),
                      _basicWidget(
                        "Contraseña", 
                        validarCampoObligatorio, 
                        (newValue) { 
                          _datos.password = newValue!; 
                        },
                        const Icon(Icons.mail),
                        isPassword: true
                      ),
                      const SizedBox(height: 10),
                      _basicWidget(
                        "Comfirma La Contraseña", 
                        validarCampoObligatorio, 
                        (newValue) { 
                          rePassword = newValue!; 
                        },
                        const Icon(Icons.mail),
                        isPassword: true
                      ),
                      const SizedBox(height: 30),
                      ElevatedButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.green,
                          minimumSize: const Size.fromHeight(50),
                        ),
                        onPressed: () async{
                          var nav =Navigator.of(context);
                          if(formKey.currentState!.validate()){
                            formKey.currentState!.save();
                            if(_datos.password == rePassword){
                              try{
                                await _controller.registerNewUser(_datos);
                                
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: 
                                      Text("Usuario Registrado Correctamente")
                                    )
                                );
                                nav.pushReplacement(MaterialPageRoute(
                                  builder: (context)=> const Reportes()
                                  ));
                              }catch(error){
                                  ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: 
                                      Text(error.toString())
                                    )
                                  );
                              }
                            }else{
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content:  Text("las contraseñas no coinciden"),
                                )
                              );
                            }
                          }
                        },
                        child: const Text("Crear Cuenta", style: TextStyle(fontSize: 20)),
                      ),
                      const SizedBox(height: 30),
                      _botonesLoginRedes(),
                      const SizedBox(height: 50),
                      TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            minimumSize: Size.zero,
                            padding: EdgeInsets.zero,
                            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          ),
                          child: const Text("¿Olvidaste tu contraseña?",
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 20))),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String? validarCampoObligatorio(String? value){

    if(value == null || value.isEmpty){
      return "el campo es obligatorio";
    }

    return null;
  } 



  Widget _campoEmail() {
    return TextFormField(
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.mail),
        border: OutlineInputBorder(),
        labelText: "Correo Electronico",
        contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      ),
    );
  }

  Widget _campoClave() {
    return TextFormField(
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.lock),
        border: OutlineInputBorder(),
        labelText: "Contraseña",
        contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      ),
    );
  }

  Widget _campoConfClave() {
    return TextFormField(
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.lock),
        border: OutlineInputBorder(),
        labelText: "Contraseña",
        contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      ),
    );
  }

  Widget _botonRegistro() {
    return ElevatedButton(
      style: TextButton.styleFrom(
        backgroundColor: Colors.green,
        minimumSize: const Size.fromHeight(50),
      ),
      onPressed: () {},
      child: const Text("Crear Cuenta", style: TextStyle(fontSize: 20)),
    );
  }

  Widget _botonesLoginRedes() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ElevatedButton(
          style: TextButton.styleFrom(
            backgroundColor: Colors.blue,
            padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 15),
          ),
          onPressed: () {},
          child: const Icon(Icons.facebook),
        ),
        ElevatedButton(
          style: TextButton.styleFrom(
            backgroundColor: Colors.red,
            padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 15),
          ),
          onPressed: () {},
          child: const Icon(Icons.g_mobiledata_sharp),
        ),
      ],
    );
  }
  ///
  ///<param name="title">titulo del campo texto</param>
  ///
  ///
  Widget  _basicWidget(
            String title,
            FormFieldValidator<String?> validate, 
            FormFieldSetter<String?> save,
            Widget icon,
            {bool isPassword = false}
            
          ){

    return TextFormField(
      obscureText: isPassword,
      decoration: InputDecoration(
        prefixIcon: icon,
        border: const OutlineInputBorder(),
        labelText: title,
        contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      ),
       onSaved: save,
        validator: validate,
    );
  }
}
