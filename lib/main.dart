import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:huecos/pages/detalle.dart';
import 'package:huecos/pages/editar.dart';
import 'package:huecos/pages/formulario.dart';
import 'package:huecos/pages/menuPrincipal.dart';
import 'package:huecos/pages/recuperar.dart';
import 'package:huecos/pages/registro.dart';
import 'package:huecos/pages/reportes.dart';
import 'package:path/path.dart';
import 'firebase_options.dart';
import 'pages/login.dart';



void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FlutterError.onError = (errorDetails) {
      FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
  };

  PlatformDispatcher.instance.onError = (exception,stackTrace){
    FirebaseCrashlytics
        .instance
            .recordError(exception, stackTrace,fatal: true);
    return true;
  };
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green[600],
      ),
      home: Login(),
    );
  }
}
