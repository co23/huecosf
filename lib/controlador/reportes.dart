import 'package:huecos/modelos/entidades/reportes.dart';
import 'package:huecos/modelos/repositorio/fb_storage.dart';
import 'package:huecos/modelos/repositorio/reportes.dart';

class ReportController{
  late ReportRepository _repository;
  late FirebaseStorageRepository _storageRepository;

  ReportController(){
    _repository = ReportRepository();
    _storageRepository = FirebaseStorageRepository();
  }
  
  Future<void> save(ReportEntity reporte) async {
    if(reporte.foto != null){
      var url = await _storageRepository.loadFile(reporte.foto!, "reportes/fotos");
      reporte.foto = url;
    }
    await _repository.newReport(reporte);
  }
  
  Future<List<ReportEntity>> listAll(String email)async{
    return await _repository.getAllReportsByEmail(email); 
  }

  Future<List<ReportEntity>> listAllReports()async{
    return await _repository.getAllReports(); 
  }
}