import 'package:huecos/controlador/request/login_request.dart';
import 'package:huecos/controlador/request/register.dart';
import 'package:huecos/controlador/response/userinfo.dart';
import 'package:huecos/modelos/entidades/usuarios.dart';
import 'package:huecos/modelos/repositorio/fb_auth.dart';
import 'package:huecos/modelos/repositorio/usuarios.dart';


class LoginController{
  late final UserRepository _userRepository;
  late final FireBaseAuthenticationRepository _authRepository;

  LoginController(){
    _userRepository = UserRepository();
    _authRepository = FireBaseAuthenticationRepository();
  }

  //Future<UserInfoResponse> validarCorreoClave(LoginRequest request) async{
  Future<String> validarCorreoClave(LoginRequest request) async{
    await _authRepository.singInEmailPassword(request.email, request.password);
    /* 
      var user = _userRepository.findByEmail(request.email);
      if(user.password != request.password){
        throw Exception("credenciales invalidas ");
      }
      return user.email!;
    */
    /* var user = _userRepository.findByEmail(request.email); */
    var user = request.email;
    
    return user; 
  }

  Future<void> registerNewUser(RegisterRequest request) async{
    await _authRepository.createEmailPasswordUser(request.email, request.password);
    /* _userRepository.guardar(UserEntity(
        email: request.email, 
        password: request.password,
        isAdmin: false
      )
    ); 
    */
  }
  
  Future<void> logOut() async{
    await _authRepository.singOut();
  }
}