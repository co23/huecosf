class UserInfoResponse{
  late String? id;
  late String? email;
  late String? isAdmin;

  UserInfoResponse({
    this.id,
    this.email,
    this.isAdmin
  });

}